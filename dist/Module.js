import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { ApolloProvider } from '@apollo/react-hooks';
import Main from './layouts/main';
import Provider from './provider/provider';
import apolloConfig from './apolloConfig';

var Module = function Module(_ref) {
    var path = _ref.match.path,
        setTitle = _ref.setTitle,
        token = _ref.token,
        isLogged = _ref.isLogged,
        urlService = _ref.urlService;

    var client = apolloConfig(urlService, token, isLogged);

    useEffect(function () {
        setTitle('Mapa - INE');
    }, []);

    return React.createElement(
        Provider,
        null,
        React.createElement(
            ApolloProvider,
            { client: client },
            React.createElement(
                Router,
                null,
                React.createElement(
                    Switch,
                    null,
                    React.createElement(Route, { exact: true, path: path === '' ? '/' : path, component: Main })
                )
            )
        )
    );
};

var propTypes = {
    match: PropTypes.object.isRequired,
    setTitle: PropTypes.func,
    isLogged: PropTypes.bool,
    urlService: PropTypes.string,
    token: PropTypes.string
};

Module.displayName = 'Modulo de INE'; // nombre de tu modulo
Module.propTypes = propTypes;
Module.defaultProps = {
    setTitle: function setTitle() {
        return {};
    },
    isLogged: false,
    urlService: '',
    token: ''
};

export default Module;