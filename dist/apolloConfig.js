import ApolloClient from 'apollo-client';
import { setContext } from 'apollo-link-context';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { HttpLink } from 'apollo-link-http';

var apolloConfig = function apolloConfig(url, token, isLogged) {
    var cache = new InMemoryCache({
        dataIdFromObject: function dataIdFromObject(result) {
            if (result._id && result.__typename) {
                if (result.__typename === 'Creator') {
                    return result.__typename + result.user_id;
                }
                return result.__typename + result._id;
            }
            return null;
        }
    });

    var httpLink = new HttpLink({
        uri: url
    });
    var authLink = setContext(function (_, _ref) {
        var headers = _ref.headers;

        if (token === '' || !isLogged) {
            return {};
        }
        return {
            headers: Object.assign({}, headers, {
                authorization: 'Bearer ' + token
            })
        };
    });
    var link = authLink.concat(httpLink);

    return new ApolloClient({
        link: link,
        cache: cache
    });
};

export default apolloConfig;