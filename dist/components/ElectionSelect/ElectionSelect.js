import React from 'react';
import PropTypes from 'prop-types';
import { Select } from 'antd';
import Map from './../MapChart';
import { MFCardList } from '../MainForces';
import VoteStats from '../VoteStats';
import PartiesList from '../PartiesList';
import styles from './styles';
import withData from './withData';

var Option = Select.Option;


var propTypes = {
    data: PropTypes.object,
    electionItems: PropTypes.array,
    fileCnfg: PropTypes.object.isRequired,
    handleChange: PropTypes.func.isRequired,
    colorMap: PropTypes.object,
    participatingParties: PropTypes.array
};

var defaultProps = {
    data: {},
    electionItems: [],
    colorMap: {},
    participatingParties: []
};

var ElectionSelect = function ElectionSelect(props) {
    var _props$data = props.data,
        voteStats = _props$data.voteStats,
        fuerzasPrc = _props$data.fuerzasPrc,
        fileCnfg = props.fileCnfg,
        handleChange = props.handleChange,
        electionItems = props.electionItems,
        participatingParties = props.participatingParties,
        colorMap = props.colorMap;


    return React.createElement(
        'div',
        Object.assign({ className: 'cmp-es-card' }, styles),
        React.createElement(
            'div',
            { className: 'cmp-es-card__map' },
            React.createElement(
                Select,
                { defaultValue: electionItems[0].election_type + '-' + electionItems[0].state + '-' + electionItems[0].year, onChange: handleChange, dropdownMatchSelectWidth: false },
                electionItems.map(function (selectItem) {
                    return React.createElement(
                        Option,
                        {
                            key: selectItem._id,
                            value: selectItem.election_type + '-' + selectItem.state + '-' + selectItem.year,
                            idplace: selectItem.state_id,
                            place: selectItem.state,
                            year: selectItem.year,
                            type: selectItem.election_type
                        },
                        'Elecciones ' + selectItem.election_type + ' de ' + selectItem.state + ' ' + selectItem.year
                    );
                })
            ),
            React.createElement(Map, {
                width: 315,
                urlFile: '' + process.env.REACT_APP_MAP_URL + fileCnfg.file + '.json',
                objectName: fileCnfg.object,
                dataID: fileCnfg.dataID[0],
                groupColor: fileCnfg.dataID[0],
                baseStyles: {
                    default: {
                        fill: 'rgba(0, 0, 0, 0.09)', stroke: 'rgba(43, 43, 43, 0.3)', strokeWidth: '0.02px', outline: 'none', cursor: 'pointer'
                    },
                    hover: { fill: 'rgba(0, 0, 0, 0.4)', outline: 'none', cursor: 'pointer' },
                    pressed: { fill: 'rgba(0, 0, 0, 0.8)', outline: 'none', cursor: 'pointer' }
                },
                colorsPerItem: colorMap
            })
        ),
        React.createElement(
            'div',
            { className: 'cmp-es-card__data' },
            React.createElement(MFCardList, { data: fuerzasPrc }),
            React.createElement(VoteStats, { data: voteStats })
        ),
        React.createElement(PartiesList, { data: participatingParties })
    );
};

ElectionSelect.propTypes = propTypes;
ElectionSelect.defaultProps = defaultProps;

export default withData(ElectionSelect);