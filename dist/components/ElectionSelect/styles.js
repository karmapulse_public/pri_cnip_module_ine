import { css } from 'glamor';

var styles = css({
    '.cmp-es-card': {
        display: 'flex',
        padding: '40px 60px 45px 45px',
        background: '#FFF',
        borderRadius: '5px',
        boxShadow: '0 1px 10px 0 rgba(0, 0, 0, 0.14)',
        justifyContent: 'space-between',
        flexWrap: 'wrap'
    },
    ' .cmp-es-card': {
        '&__data': {
            width: '605px',
            ' .cmp-vs-container': {
                marginTop: '35px'
            }
        },
        '&__map': {
            width: '310px'
        }
    },
    ' .cmp-pl-container': {
        marginTop: '100px'
    }
});

export default styles;