var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

import React, { useContext } from 'react';
import { Spin } from 'antd';
import { useLazyQuery } from '@apollo/react-hooks';
import { GET_ELECTION } from '../../layouts/main/queries';
import Context from '../../provider/context';
import { CHANGE_PLACE } from '../../provider/actions';

var withData = function withData(Component) {
    return function (cmpProps) {
        var _useContext = useContext(Context),
            _useContext2 = _slicedToArray(_useContext, 2),
            redux = _useContext2[0],
            dispatch = _useContext2[1];

        var loading = redux.loading,
            voteStats = redux.voteStats,
            winnerParties = redux.winnerParties,
            fuerzasPrc = redux.fuerzasPrc,
            electionItems = redux.electionItems,
            fileCnfg = redux.fileCnfg,
            participatingParties = redux.participatingParties,
            list = redux.list;

        var _useLazyQuery = useLazyQuery(GET_ELECTION, {
            onCompleted: function onCompleted(data) {
                dispatch({
                    type: CHANGE_PLACE,
                    payload: {
                        selectedElection: data.getElectionList.electionList[0],
                        selectedPlace: mapData.variables.preData
                    }
                });
            }
        }),
            _useLazyQuery2 = _slicedToArray(_useLazyQuery, 2),
            getElection = _useLazyQuery2[0],
            mapData = _useLazyQuery2[1];

        var handleChange = function handleChange(v, e) {
            var props = e.props;
            var idplace = props.idplace,
                type = props.type,
                year = props.year;

            getElection({
                variables: {
                    filter: 'election_type=' + type + ',state_id=' + idplace + ',year=' + year,
                    preData: props
                }
            });
        };

        if (loading) {
            return React.createElement(
                'div',
                {
                    style: {
                        display: 'flex',
                        width: '100%',
                        height: '100%',
                        alignItems: 'center',
                        justifyContent: 'center'
                    }
                },
                React.createElement(Spin, {
                    tip: 'Cargando',
                    size: 'large'
                })
            );
        }

        var colorMap = {};
        list.forEach(function (iPlace) {
            colorMap[iPlace.id] = { fill: iPlace.political_forces[0].color };
        });

        return React.createElement(Component, Object.assign({}, cmpProps, {
            data: {
                voteStats: voteStats,
                winnerParties: winnerParties,
                fuerzasPrc: fuerzasPrc
            },
            participatingParties: participatingParties,
            electionItems: electionItems,
            fileCnfg: fileCnfg,
            colorMap: colorMap,
            handleChange: handleChange
        }));
    };
};

export default withData;