import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Tooltip } from 'antd';

import moduleStyles, { barStyles, toolTipStyles } from './styles';
import wrapperText from '../../../helpers/wrapperText';
import numberWithCommas from '../../../helpers/number';
import uniqueId from '../../../helpers/uniqueId';

var MFBarChat = function MFBarChat(props) {
    var data = props.data,
        place = props.place;

    var maxWidth = 365;
    return React.createElement(
        Fragment,
        null,
        React.createElement(
            'ul',
            Object.assign({ className: 'cmp-mf-chart' }, moduleStyles),
            data.map(function (item) {
                return React.createElement(
                    'li',
                    {
                        key: uniqueId(),
                        className: 'cmp-mf-chart__item'
                    },
                    React.createElement(
                        Tooltip,
                        {
                            overlayClassName: toolTipStyles(item.color).toString(),
                            title: React.createElement(
                                Fragment,
                                null,
                                React.createElement(
                                    'h6',
                                    null,
                                    place
                                ),
                                React.createElement(
                                    'ul',
                                    null,
                                    React.createElement(
                                        'li',
                                        null,
                                        React.createElement(
                                            'span',
                                            null,
                                            wrapperText(item.political_entity.join(', '), 20, 4)
                                        ),
                                        React.createElement(
                                            'span',
                                            null,
                                            item.percentage + ' %'
                                        )
                                    ),
                                    React.createElement(
                                        'li',
                                        null,
                                        React.createElement(
                                            'span',
                                            null,
                                            'Total de votos:'
                                        ),
                                        React.createElement(
                                            'span',
                                            null,
                                            numberWithCommas(item.votes || item.total)
                                        )
                                    )
                                )
                            )
                        },
                        React.createElement('div', Object.assign({
                            className: 'cmp-mf-chart__item__bar'
                        }, barStyles(item.color, maxWidth * item.percentage / 100)))
                    ),
                    React.createElement(
                        'p',
                        {
                            className: 'cmp-mf-chart__item__data'
                        },
                        React.createElement(
                            'span',
                            null,
                            numberWithCommas(item.votes || item.total),
                            ' votos'
                        ),
                        React.createElement(
                            'span',
                            null,
                            item.percentage + '%'
                        )
                    )
                );
            })
        )
    );
};

var propTypes = {
    data: PropTypes.array,
    place: PropTypes.string
};

var defaultProps = {
    data: [],
    place: ''
};

MFBarChat.propTypes = propTypes;
MFBarChat.defaultProps = defaultProps;

export default MFBarChat;