import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

import moduleStyles, { cardStyles } from './styles';
import withData from '../withData';
import wrapperText from '../../../helpers/wrapperText';
import numberWithCommas from '../../../helpers/number';
import uniqueId from '../../../helpers/uniqueId';

var MFCardList = function MFCardList(props) {
    var data = props.data,
        participants = props.participants,
        type = props.type;

    var dict = ['Primera', 'Segunda', 'Tercera'];
    var dictType = {
        gobernatura: 'votos',
        'diputaciones locales': 'distritos',
        municipios: 'municipios'
    };

    return React.createElement(
        'ul',
        Object.assign({ className: 'cmp-mf-cards' }, moduleStyles),
        data.map(function (item, index) {
            return React.createElement(
                'li',
                {
                    key: uniqueId(),
                    className: 'cmp-mf-cards__item'
                },
                React.createElement(
                    'span',
                    { className: 'cmp-mf-cards__item__title' },
                    dict[index] + ' fuerza'
                ),
                React.createElement('div', Object.assign({
                    className: 'cmp-mf-cards__item__bar'
                }, cardStyles(item.color))),
                React.createElement(
                    'p',
                    {
                        className: 'cmp-mf-cards__item__name'
                    },
                    wrapperText(item.political_entity.join(', '), 40)
                ),
                React.createElement(
                    'p',
                    {
                        className: 'cmp-mf-cards__item__data'
                    },
                    React.createElement(
                        'span',
                        null,
                        numberWithCommas(item.votes || item.victories),
                        ' ',
                        dictType[type.toLowerCase()]
                    ),
                    React.createElement(
                        'span',
                        null,
                        item.percentage.toFixed(2) + '%'
                    ),
                    React.createElement(
                        'span',
                        null,
                        'del resultado electoral'
                    )
                )
            );
        }),
        React.createElement(
            'li',
            null,
            participants === null ? null : React.createElement(
                Fragment,
                null,
                React.createElement(
                    'span',
                    null,
                    'Total de contendientes registrados en la elecci\xF3n:'
                ),
                React.createElement(
                    'span',
                    null,
                    ' ' + participants
                )
            )
        )
    );
};

var propTypes = {
    data: PropTypes.array,
    participants: PropTypes.number,
    type: PropTypes.string
};

var defaultProps = {
    data: [],
    participants: 0,
    type: ''
};

MFCardList.propTypes = propTypes;
MFCardList.defaultProps = defaultProps;

export default withData(MFCardList);