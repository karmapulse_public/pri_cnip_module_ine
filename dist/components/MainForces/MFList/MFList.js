import React from 'react';
import PropTypes from 'prop-types';
import moduleStyles, { dotStyles } from './styles';
import wrapperText from '../../../helpers/wrapperText';
import uniqueid from '../../../helpers/uniqueId';
// import withData from '../withData';

var MFList = function MFList(props) {
    var data = props.data;


    return React.createElement(
        'ul',
        Object.assign({ className: 'cmp-mf-list' }, moduleStyles),
        data.map(function (item, index) {
            return React.createElement(
                'li',
                {
                    key: uniqueid(),
                    className: 'cmp-mf-list__item'
                },
                React.createElement(
                    'span',
                    null,
                    index + 1 + 'ra. fuerza'
                ),
                React.createElement(
                    'span',
                    dotStyles(item.color),
                    wrapperText(item.political_entity.join(', '), 30)
                )
            );
        })
    );
};

var propTypes = {
    data: PropTypes.array
};

var defaultProps = {
    data: []
};

MFList.propTypes = propTypes;
MFList.defaultProps = defaultProps;

export default MFList;