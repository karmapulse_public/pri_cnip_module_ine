import { css } from 'glamor';

export default css({
    '.cmp-mf-list': {
        display: 'inline-flex',
        width: '405px',
        height: '145px',
        padding: '15px 40px',
        backgroundColor: '#f3f3f3',
        flexDirection: 'column',
        justifyContent: 'space-between'
    },
    ' .cmp-mf-list': {
        '&__item': {
            fontSize: 14,
            fontFamily: 'Roboto',
            fontWeight: 'normal',
            fontStretch: 'normal',
            fontStyle: 'normal',
            letterSpacing: 'normal',
            textAlign: 'left',
            color: '#000000'
        },
        '&__item > span:first-child': {
            opacity: 0.5
        },
        '&__item > span:last-child': {
            marginLeft: 45
        }
    }
});

export var dotStyles = function dotStyles(bgColor) {
    return css({
        position: 'relative',
        '&:after': {
            content: '""',
            position: 'absolute',
            top: '50%',
            right: 'calc(100% + 10px)',
            width: 8,
            height: 8,
            backgroundColor: bgColor,
            borderRadius: '50%',
            transform: 'translate(0, -50%)'
        }
    });
};