var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

import React, { useContext } from 'react';

import Context from '../../provider/context';

var withData = function withData(Component) {
    return function (props) {
        var _useContext = useContext(Context),
            _useContext2 = _slicedToArray(_useContext, 1),
            redux = _useContext2[0];

        var data = props.data;
        var partiesList = redux.partiesList,
            voteStats = redux.voteStats,
            type = redux.election.type;

        var mappedData = data.map(function (i) {
            var party = partiesList.find(function (e) {
                return e.acronym === i.political_entity[0];
            });
            return Object.assign({}, i, {
                color: '#' + (party ? party.color : 'e9e9e9')
            });
        });

        return React.createElement(Component, {
            data: mappedData,
            type: type,
            participants: voteStats.participants
        });
    };
};

export default withData;