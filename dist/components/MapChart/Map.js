import React from 'react';
import PropTypes from 'prop-types';
import { ComposableMap, ZoomableGroup, Geographies, Geography } from 'react-simple-maps';
import ReactTooltip from 'react-tooltip';
import { Motion, spring } from 'react-motion';
import estilos from './styles';
import uniqueId from '../../helpers/uniqueId';
import widthData from './withData';

var propTypes = {
    // Functional props
    width: PropTypes.number,
    height: PropTypes.number,
    scale: PropTypes.number,
    // Info props
    data: PropTypes.object,
    objectName: PropTypes.string,
    dataID: PropTypes.string,
    // Style props
    groupColor: PropTypes.string,
    baseStyles: PropTypes.objectOf(PropTypes.object),
    colorsPerItem: PropTypes.objectOf(PropTypes.object),
    transformTopo: PropTypes.func.isRequired,
    zoom: PropTypes.number.isRequired,
    center: PropTypes.arrayOf(PropTypes.number).isRequired,
    ctrZoom: PropTypes.number.isRequired,
    setCtrZoom: PropTypes.func.isRequired,
    setTooltip: PropTypes.func
};

var defaultProps = {
    width: 600,
    height: 400,
    scale: 150,
    data: {},
    objectName: null,
    dataID: 'id',
    groupColor: 'group',
    baseStyles: {},
    colorsPerItem: {},
    setTooltip: function setTooltip() {
        return {};
    }
};

var setColorByMap = function setColorByMap(item, key, baseStyles, itemsStyles) {
    var styles = Object.keys(item).includes(key) ? itemsStyles[item[key]] : {};
    var algo = Object.assign({}, baseStyles, {
        default: Object.assign({}, baseStyles.default, styles)
    });
    return algo;
};

var Map = function Map(_ref) {
    var width = _ref.width,
        height = _ref.height,
        scale = _ref.scale,
        zoom = _ref.zoom,
        center = _ref.center,
        data = _ref.data,
        objectName = _ref.objectName,
        groupColor = _ref.groupColor,
        baseStyles = _ref.baseStyles,
        colorsPerItem = _ref.colorsPerItem,
        dataID = _ref.dataID,
        transformTopo = _ref.transformTopo,
        ctrZoom = _ref.ctrZoom,
        setTooltip = _ref.setTooltip;
    return React.createElement(
        'div',
        estilos,
        React.createElement(
            Motion,
            {
                defaultStyle: {
                    zoom: zoom,
                    x: center[0],
                    y: center[1]
                },
                style: {
                    zoom: spring(ctrZoom < 0 ? zoom : ctrZoom, { stiffness: 100, damping: 15 }),
                    x: spring(center[0], { stiffness: 100, damping: 15 }),
                    y: spring(center[1], { stiffness: 100, damping: 15 })
                }
            },
            function (perspective) {
                return React.createElement(
                    ComposableMap,
                    {
                        width: width,
                        height: height,
                        projection: 'mercator',
                        projectionConfig: {
                            scale: scale
                        }
                    },
                    React.createElement(
                        ZoomableGroup,
                        {
                            center: [perspective.x, perspective.y],
                            zoom: perspective.zoom
                        },
                        React.createElement(
                            Geographies,
                            { geography: transformTopo(data, objectName) },
                            function (geographies, projection) {
                                return geographies.map(function (item) {
                                    return React.createElement(Geography, {
                                        key: uniqueId()
                                        // onClick={(a, b, c, d, e) => {
                                        //     // const alh = createPerspective(item);
                                        //     // console.log(data, item, a, b, c, d, e);
                                        // }}
                                        , onMouseOver: function onMouseOver() {
                                            ReactTooltip.rebuild();
                                        },
                                        onFocus: function onFocus() {
                                            ReactTooltip.rebuild();
                                        },
                                        geography: item,
                                        projection: projection,
                                        style: setColorByMap(item.properties, groupColor, baseStyles, colorsPerItem),
                                        'data-tip': item.properties[dataID]
                                    });
                                });
                            }
                        )
                    )
                );
            }
        ),
        React.createElement(ReactTooltip, {
            type: 'info',
            className: 'map__tooltip',
            effect: 'solid',
            place: 'right',
            getContent: function getContent(item) {
                return setTooltip(item);
            }
        })
    );
};

Map.propTypes = propTypes;
Map.defaultProps = defaultProps;

export default widthData(Map);