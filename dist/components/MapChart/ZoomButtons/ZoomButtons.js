import React from 'react';
import PropTypes from 'prop-types';
import { faPlus, faMinus } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

var propTypes = {
    // Functional props
    setCtrZoom: PropTypes.func,
    zoom: PropTypes.number
};

var defaultProps = {
    setCtrZoom: function setCtrZoom() {
        return {};
    },
    zoom: 7
};

var Map = function Map(_ref) {
    var setCtrZoom = _ref.setCtrZoom,
        zoom = _ref.zoom;
    return React.createElement(
        'div',
        { className: 'map__zoom-buttons' },
        React.createElement(
            'button',
            {
                onClick: function onClick() {
                    setCtrZoom(zoom + 10);
                }
            },
            React.createElement(FontAwesomeIcon, { icon: faPlus })
        ),
        React.createElement(
            'button',
            {
                onClick: function onClick() {
                    setCtrZoom(zoom - 10);
                }
            },
            React.createElement(FontAwesomeIcon, { icon: faMinus })
        )
    );
};

Map.propTypes = propTypes;
Map.defaultProps = defaultProps;

export default Map;