import { css } from 'glamor';

export default css({
    position: 'relative',
    width: '100%',
    maxWidth: 980,
    margin: '0px auto',
    ' .map__tooltip': {
        minWidth: '200px',
        padding: '10px',
        color: '#FFF',
        fontSize: '10px',
        backgroundColor: 'rgba(0, 0, 0, 0.8)',
        ' > h6': {
            color: '#FFF',
            fontWeight: 700,
            marginBottom: '0.8em'
        },
        ' > ul': {
            marginBottom: 0
        },
        ' > ul > li': {
            display: 'flex',
            alignItems: 'flex-start',
            justifyContent: 'space-between',
            '&:last-child': {
                marginTop: '1em'
            },
            ' > span:first-child': {
                maxWidth: '150px'
            },
            ' > span:last-child': {
                fontWeight: 700
            }
        }
    },
    ' .map__zoom-buttons': {
        position: 'absolute',
        top: '15px',
        right: '10px',
        ' > button': {
            display: 'block',
            border: 'solid 1px rgba(255, 255, 255, 0.1)',
            outline: 'none',
            cursor: 'pointer',
            background: 'rgba(255, 255, 255, 0.3)',
            transition: 'all ease 0.25s',
            '&:hover': {
                background: 'rgba(255, 255, 255, 0.7)',
                border: 'solid 1px rgba(223, 229, 230, 1)'
            }
        }
    },
    ' .place-right:after, .place-left:after, .place-top:after, .place-bottom:after': {
        display: 'none'
    }
});