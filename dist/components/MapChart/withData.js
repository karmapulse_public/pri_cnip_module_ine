var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

import React, { useState, useEffect, useContext } from 'react';
import { Spin } from 'antd';
import fetch from 'isomorphic-fetch';
import { feature, bbox } from 'topojson-client';
import Tooltip from '../TooltipPlace';
import Context from '../../provider/context';

var withData = function withData(Component) {
    return function (props) {
        var _useContext = useContext(Context),
            _useContext2 = _slicedToArray(_useContext, 1),
            redux = _useContext2[0];

        var _props$urlFile = props.urlFile,
            urlFile = _props$urlFile === undefined ? null : _props$urlFile,
            _props$width = props.width,
            width = _props$width === undefined ? 600 : _props$width,
            _props$height = props.height,
            height = _props$height === undefined ? 500 : _props$height;

        var _useState = useState({}),
            _useState2 = _slicedToArray(_useState, 2),
            topoData = _useState2[0],
            setTopoData = _useState2[1];

        var _useState3 = useState(0),
            _useState4 = _slicedToArray(_useState3, 2),
            status = _useState4[0],
            setStatus = _useState4[1]; // x < 0: Error; x = 0: Loading; x > 0: Data


        var _useState5 = useState(-1),
            _useState6 = _slicedToArray(_useState5, 2),
            ctrZoom = _useState6[0],
            setCtrZoom = _useState6[1];

        // GET DATA - TOPOJSONS


        useEffect(function () {
            setStatus(0);
            fetch(urlFile).then(function (response) {
                return response.json();
            }).then(function (topoState) {
                setTopoData(topoState);
                setStatus(1);
            }).catch(function () {
                setStatus(-1);
            });
        }, [urlFile, setTopoData, setStatus]);

        /* Functions */
        var transformTopo = function transformTopo(topo, key) {
            if (!topo || Object.keys(topo).length === 0) return [];
            return feature(topo, topo.objects[key] || {}).features;
        };

        var createPerspective = function createPerspective(topo) {
            if (Object.keys(topo).length === 0) return { center: [19.432608, -99.133209], zoom: 7 };
            var newTopo = Object.assign({}, topo);
            newTopo.bbox = newTopo.bbox || bbox(newTopo);
            var dx = newTopo.bbox[2] + newTopo.bbox[0];
            var dy = newTopo.bbox[1] + newTopo.bbox[3];
            var ddx = newTopo.bbox[2] - newTopo.bbox[0];
            var ddy = newTopo.bbox[3] - newTopo.bbox[1];
            return {
                center: [dx / 2, dy / 2],
                zoom: 0.3 / Math.max(ddx / width, ddy / height)
            };
        };

        var setTooltip = function setTooltip(id) {
            if (!id) return null;
            var list = redux.list;

            var itemMap = list.find(function (i) {
                return i.id === parseInt(id, 10);
            });
            if (!itemMap) return null;
            var percentage = (itemMap.political_forces[0].votes || itemMap.political_forces[0].victories) * 100 / itemMap.total_votes;
            return React.createElement(Tooltip, {
                place: itemMap.denomination,
                porcentage: percentage.toFixed(2),
                total: itemMap.total_votes,
                partidos: itemMap.political_forces[0].political_entity,
                color: itemMap.political_forces[0].color
            });
        };
        /* Functions */

        var tempPers = createPerspective(topoData);
        var zoom = tempPers.zoom;
        var center = tempPers.center;

        zoom = ctrZoom < 0 ? zoom : ctrZoom;

        if (status < 0) {
            return null;
        }
        if (status === 0) {
            return React.createElement(
                'div',
                {
                    style: {
                        display: 'flex',
                        width: '100%',
                        height: '100%',
                        alignItems: 'center',
                        justifyContent: 'center'
                    }
                },
                React.createElement(Spin, {
                    tip: 'Cargando',
                    size: 'large'
                })
            );
        }
        if (status > 0) {
            return React.createElement(Component, Object.assign({
                data: topoData,
                zoom: zoom,
                center: center,
                transformTopo: transformTopo,
                ctrZoom: ctrZoom,
                setCtrZoom: setCtrZoom,
                setTooltip: setTooltip
            }, props));
        }
        return null;
    };
};

export default withData;