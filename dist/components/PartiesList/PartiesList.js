import React from 'react';
import PropTypes from 'prop-types';

import uniqueId from '../../helpers/uniqueId';
import styles, { partyStyles } from './styles';

var propTypes = {
    data: PropTypes.arrayOf(PropTypes.object)
};

var defaultProps = {
    data: [{}]
};

var PartiesList = function PartiesList(props) {
    var data = props.data;

    return React.createElement(
        'ul',
        Object.assign({ className: 'cmp-pl-container' }, styles),
        data.map(function (item) {
            return React.createElement(
                'li',
                Object.assign({
                    className: 'cmp-pl-container__item'
                }, partyStyles(item.color), {
                    key: uniqueId()
                }),
                item.political_entity.join(', ')
            );
        })
    );
};

PartiesList.propTypes = propTypes;
PartiesList.defaultProps = defaultProps;

export default PartiesList;