import { css } from 'glamor';

var styles = css({
    '.cmp-pl-container': {
        display: 'flex',
        width: '100%'
    },
    ' .cmp-pl-container': {
        '&__item:not(:last-child)': {
            marginRight: '25px'
        }
    }
});

export var partyStyles = function partyStyles(bgColor) {
    return css({
        '.cmp-pl-container__item': {
            position: 'relative',
            paddingLeft: '15px',
            fontSize: '12px',
            textTransform: 'uppercase',
            ':before': {
                content: '""',
                position: 'absolute',
                top: '50%',
                left: '0px',
                width: '8px',
                height: '8px',
                borderRadius: '50%',
                background: bgColor,
                transform: 'translate(0, -50%)'
            }
        }
    });
};

export default styles;