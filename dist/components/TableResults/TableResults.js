import React from 'react';
import PropTypes from 'prop-types';
import { Table } from 'antd';
import withData from './withData';
import styles from './styles';

var TableResults = function TableResults(props) {
    var data = props.data,
        columns = props.columns;


    return React.createElement(Table, Object.assign({
        bordered: true,
        align: 'center',
        columns: columns,
        dataSource: data,
        rowKey: 'id',
        pagination: {
            showSizeChanger: true,
            hideOnSinglePage: true,
            locale: { items_per_page: ' registros ' }
        }
    }, styles));
};

var propTypes = {
    columns: PropTypes.arrayOf(PropTypes.object),
    data: PropTypes.array
};

var defaultProps = {
    columns: [],
    data: []
};

TableResults.propTypes = propTypes;
TableResults.defaultProps = defaultProps;

export default withData(TableResults);