import { css } from 'glamor';

export default css({
    boxShadow: '0 1px 10px 0 rgba(0, 0, 0, 0.14)',
    ' .ant-table-tbody > tr': {
        ' > td': {
            paddingTop: '0px',
            background: '#FFF'
        },
        ' > td:nth-child(2)': {
            padding: '0px'
        },
        ' > td:not(:nth-child(2))': {
            verticalAlign: 'middle'
        }
    },
    ' .tbl-rslts__name': {
        display: 'flex',
        height: '100%',
        color: '#000',
        textTransform: 'uppercase',
        alignItems: 'center'
    },
    ' .tbl-rslts__main-forces': {
        display: 'flex',
        alignItems: 'center'
    },
    ' .tbl-rslts__total-votes': {
        paddingRight: '50px',
        fontWeight: 700,
        color: '#000',
        fontSize: '18px',
        textAlign: 'right'
    }
});