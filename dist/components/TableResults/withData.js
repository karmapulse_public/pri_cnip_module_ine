var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

import React, { useContext } from 'react';
import { MFList, MFBarChart } from '../MainForces';
import numberWithCommas from '../../helpers/number';
// import wrapperText from '../../helpers/wrapperText';
import Context from '../../provider/context';

var withData = function withData(Component) {
    return function () {
        var _useContext = useContext(Context),
            _useContext2 = _slicedToArray(_useContext, 1),
            redux = _useContext2[0];

        var list = redux.list,
            type = redux.election.type;

        var dictType = {
            gobernatura: 'Municipio',
            'diputaciones locales': 'Distrito Local',
            municipios: 'Municipio'
        };
        var columns = [{
            title: dictType[type.toLowerCase()],
            dataIndex: 'denomination',
            key: 'denomination',
            width: 175,
            render: function render(name, row) {
                return React.createElement(
                    'p',
                    { className: 'tbl-rslts__name' },
                    row.id + '. ' + name
                );
            },
            sorter: function sorter(a, b) {
                return a.id - b.id;
            }
        }, {
            title: 'Fuerza electoral',
            dataIndex: 'political_forces',
            key: 'id',
            width: 765,
            render: function render(objectData, row) {
                return React.createElement(
                    'div',
                    { className: 'tbl-rslts__main-forces' },
                    React.createElement(MFList, { data: objectData }),
                    React.createElement(MFBarChart, { data: objectData, place: row.denomination })
                );
            }
        }, {
            title: 'Total de votos',
            dataIndex: 'total_votes',
            key: 'total_votes',
            render: function render(number) {
                return React.createElement(
                    'p',
                    { className: 'tbl-rslts__total-votes' },
                    numberWithCommas(number)
                );
            },
            sorter: function sorter(a, b) {
                return a.total_votes - b.total_votes;
            }
        }];

        return React.createElement(Component, {
            data: list,
            columns: columns
        });
    };
};

export default withData;