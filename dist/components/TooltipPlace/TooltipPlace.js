import React from 'react';
import PropTypes from 'prop-types';
import wrapperText from '../../helpers/wrapperText';
import numberWithCommas from '../../helpers/number';
import styles from './styles';

var TooltipPlace = function TooltipPlace(props) {
    var place = props.place,
        partidos = props.partidos,
        porcentage = props.porcentage,
        total = props.total,
        color = props.color;


    return React.createElement(
        'div',
        styles(color),
        React.createElement(
            'h6',
            null,
            place
        ),
        React.createElement(
            'ul',
            null,
            React.createElement(
                'li',
                null,
                React.createElement(
                    'span',
                    null,
                    wrapperText(partidos.join(', '), 20, 4)
                ),
                React.createElement(
                    'span',
                    null,
                    porcentage + ' %'
                )
            ),
            React.createElement(
                'li',
                null,
                React.createElement(
                    'span',
                    null,
                    'Total de votos:'
                ),
                React.createElement(
                    'span',
                    null,
                    numberWithCommas(total)
                )
            )
        )
    );
};

var propTypes = {
    place: PropTypes.string,
    partidos: PropTypes.arrayOf(PropTypes.string),
    porcentage: PropTypes.string,
    total: PropTypes.number,
    color: PropTypes.string
};

var defaultProps = {
    place: 'Localidad',
    partidos: ['Partidos'],
    porcentage: '0',
    total: 0,
    color: '#FFF'
};

TooltipPlace.propTypes = propTypes;
TooltipPlace.defaultProps = defaultProps;

export default TooltipPlace;