import { css } from 'glamor';

var toolTipStyles = function toolTipStyles(bgColor) {
    return css({
        minWidth: '235px',
        padding: 5,
        ' > h6': {
            marginBottom: '5px',
            fontSize: '16px',
            color: '#FFF',
            textTransform: 'capitalize'
        },
        ' > ul': {
            paddingLeft: 15,
            fontSize: '12px',
            color: 'rgba(255, 255, 255, 0.8)',
            ' > li': {
                position: 'relative',
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'space-between'
            }
        },
        ' > ul > li:first-child': {
            marginBottom: '20px',
            ':before': {
                content: '""',
                position: 'absolute',
                top: '10%',
                right: 'calc(100% + 8px)',
                width: '8px',
                height: '8px',
                backgroundColor: bgColor,
                borderRadius: '50%'
            }
        }
    });
};

export default toolTipStyles;