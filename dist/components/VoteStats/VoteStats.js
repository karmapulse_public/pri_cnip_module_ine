import React from 'react';
import PropTypes from 'prop-types';

import styles from './styles';
import numberWithCommas from '../../helpers/number';

var VoteStats = function VoteStats(props) {
    var data = props.data;

    return React.createElement(
        'ul',
        Object.assign({
            className: 'cmp-vs-container'
        }, styles),
        React.createElement(
            'li',
            {
                className: 'cmp-vs-container__item'
            },
            React.createElement(
                'h4',
                null,
                'Total de Votos'
            ),
            React.createElement(
                'span',
                null,
                numberWithCommas(data.total)
            )
        ),
        React.createElement(
            'li',
            {
                className: 'cmp-vs-container__item'
            },
            React.createElement(
                'h4',
                null,
                'Votaci\xF3n v\xE1lida'
            ),
            React.createElement(
                'span',
                null,
                numberWithCommas(data.totalValido)
            )
        ),
        React.createElement(
            'li',
            {
                className: 'cmp-vs-container__item'
            },
            React.createElement(
                'h4',
                null,
                'Lista Nominal'
            ),
            React.createElement(
                'span',
                null,
                numberWithCommas(data.nominal)
            )
        ),
        React.createElement(
            'li',
            {
                className: 'cmp-vs-container__item'
            },
            React.createElement(
                'h4',
                null,
                'Porcentaje de Participaci\xF3n'
            ),
            React.createElement(
                'span',
                null,
                data.participacion.toFixed(2),
                '%'
            )
        )
    );
};

var propTypes = {
    data: PropTypes.objectOf(PropTypes.number)
};

var defaultProps = {
    data: {}
};

VoteStats.propTypes = propTypes;
VoteStats.defaultProps = defaultProps;

export default VoteStats;