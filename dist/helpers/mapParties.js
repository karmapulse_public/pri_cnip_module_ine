var mapParties = function mapParties(coalitions, keyList, keyValue, value, type) {
    if (type === 'coalition') {
        return coalitions.find(function (item) {
            return item[keyList] === value;
        })[keyValue];
    }
    if (type === 'political_party') {
        return [value];
    }
};

export default mapParties;