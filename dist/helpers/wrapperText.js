import React, { Fragment } from 'react';
import uniqueid from './uniqueId';

var wrapperText = function wrapperText(rawText, maxWidth) {
    var maxHeight = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 1;

    if (rawText.length <= maxWidth) return rawText;

    var i = 0;
    var f = 0;
    var listSegment = [];
    // Pre procesing
    while (i + maxWidth < rawText.length && f < maxHeight) {
        var rawSegment = rawText.substring(i, i + maxWidth);
        var j = i + maxWidth;
        if (rawText[j] !== ' ' && rawText[j - 1] !== ' ') {
            j -= 1;
            while (rawText[j] !== ' ') {
                j -= 1;
            }
            j += 1;
        }
        var remainder = i - (j - maxWidth);
        listSegment.push(remainder > 0 ? rawText.substring(i, i + (maxWidth - remainder)) : rawSegment);
        i = remainder > 0 ? i - remainder : i;
        i += maxWidth;
        f += 1;
    }
    if (f < maxHeight) {
        listSegment.push(rawText.substring(i, i + maxWidth));
    }
    f = 0;
    var lastItem = listSegment.pop();
    return React.createElement(
        Fragment,
        null,
        listSegment.map(function (item) {
            return React.createElement(
                Fragment,
                { key: uniqueid() },
                item,
                React.createElement('br', null)
            );
        }),
        lastItem + '...'
    );
};

export default wrapperText;