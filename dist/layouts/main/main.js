import React from 'react';
import PropTypes from 'prop-types';
import TableResults from '../../components/TableResults';
import ElectionSelect from '../../components/ElectionSelect';
import styles from './styles';
import withData from './withData';

var propTypes = {
    place: PropTypes.string,
    type: PropTypes.string,
    year: PropTypes.number
};

var defaultProps = {
    place: '',
    type: '',
    year: 0
};

var main = function main(props) {
    var place = props.place,
        type = props.type,
        year = props.year;

    var dictType = {
        gobernatura: 'municipio',
        'diputaciones locales': 'distrito',
        municipios: 'municipio'
    };
    return React.createElement(
        'div',
        Object.assign({ className: 'lyt-main' }, styles),
        React.createElement(
            'div',
            { className: 'lyt-main__title' },
            React.createElement(
                'h1',
                null,
                'ELECCI\xD3N ' + place + ' ' + (year > 0 ? year : '')
            )
        ),
        React.createElement(ElectionSelect, null),
        React.createElement(
            'div',
            { className: 'lyt-main__subtitle' },
            React.createElement(
                'h2',
                null,
                'Detalles de la elecci\xF3n por locaci\xF3n'
            ),
            React.createElement(
                'p',
                null,
                'Lista de las principales fuerzas por n\xFAmero de votos, porcentaje de votaci\xF3n y total de votos del ' + dictType[type.toLowerCase()] + '.'
            )
        ),
        React.createElement(TableResults, null)
    );
};

main.propTypes = propTypes;
main.defaultProps = defaultProps;

export default withData(main);