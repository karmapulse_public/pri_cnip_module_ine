import { css } from 'glamor';

var styles = css({
    '.lyt-main': {
        width: '100vw',
        height: '100vh',
        padding: '30px',
        background: '#f3f3f3',
        overflow: 'scroll',
        ' > div': {
            width: '1150px',
            '&:nth-child(2n)': {
                margin: '0px auto'
            }
        }
    },
    ' .lyt-main': {
        '&__title': {
            margin: '0px auto 15px',
            fontSize: '28px',
            fontWeight: 300,
            color: '#000',
            textTransform: 'uppercase'
        },
        '&__subtitle': {
            margin: '50px auto 15px',
            fontWeight: 300,
            color: '#000',
            lineHeight: 1.25,
            ' > h2': {
                textTransform: 'uppercase'
            },
            ' > p': {
                fontSize: '14px',
                fontWeight: 400
            }
        }
    }
});

export default styles;