var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

import React, { useContext } from 'react';

import { useQuery, useLazyQuery } from '@apollo/react-hooks';
import Context from '../../provider/context';
import { GET_ELECTION_LIST, GET_ELECTION } from './queries';
import { INIT_DATA } from '../../provider/actions';

var withData = function withData(Component) {
    return function () {
        var _useContext = useContext(Context),
            _useContext2 = _slicedToArray(_useContext, 2),
            redux = _useContext2[0],
            dispatch = _useContext2[1];

        var _useLazyQuery = useLazyQuery(GET_ELECTION, {
            onCompleted: function onCompleted(data) {
                dispatch({
                    type: INIT_DATA,
                    payload: {
                        selectedElection: data.getElectionList.electionList[0],
                        selectedPlace: mapData.variables.preData,
                        electionItems: mapData.variables.electionItems,
                        stateList: mapData.variables.stateList,
                        partiesList: mapData.variables.partiesList
                    }
                });
            }
        }),
            _useLazyQuery2 = _slicedToArray(_useLazyQuery, 2),
            getElection = _useLazyQuery2[0],
            mapData = _useLazyQuery2[1];

        useQuery(GET_ELECTION_LIST, {
            fetchPolicy: 'network-only',
            variables: { sort: 'state_id,year' },
            onCompleted: function onCompleted(data) {
                if (data) {
                    var electionList = data.getElectionList.electionList,
                        stateList = data.getStateList.stateList,
                        getPoliticalPartiesList = data.getPoliticalPartiesList;
                    var _electionList$ = electionList[0],
                        election_type = _electionList$.election_type,
                        state_id = _electionList$.state_id,
                        _year = _electionList$.year;

                    var selectData = electionList.map(function (elecItem) {
                        return Object.assign({}, elecItem, {
                            state: stateList.find(function (i) {
                                return i.id === elecItem.state_id;
                            }).denomination
                        });
                    });
                    getElection({
                        variables: {
                            filter: 'election_type=' + election_type + ',state_id=' + state_id + ',year=' + _year,
                            preData: electionList[0],
                            electionItems: selectData,
                            partiesList: getPoliticalPartiesList,
                            stateList: stateList
                        }
                    });
                }
            }
        });

        var _redux$election = redux.election,
            place = _redux$election.place,
            type = _redux$election.type,
            year = _redux$election.year;


        return React.createElement(Component, {
            place: place,
            type: type,
            year: year
        });
    };
};

export default withData;