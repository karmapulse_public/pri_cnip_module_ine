import React from 'react';
import PropTypes from 'prop-types';
import Context from './context';
import reducer from './reducer';
import store from './store';

var Provider = function Provider(props) {
    var contextValue = React.useReducer(reducer, store);
    return React.createElement(
        Context.Provider,
        { value: contextValue },
        props.children
    );
};

Provider.propTypes = {
    children: PropTypes.any.isRequired
};

export default Provider;