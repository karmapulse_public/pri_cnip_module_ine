import store from './store';

import { INIT_DATA, CHANGE_PLACE } from './actions';

var reducer = function reducer(state, action) {
    var dictType = {
        gobernatura: { entidad: 'municipio', dataID: 'municipio' },
        'diputaciones locales': { entidad: 'local', dataID: 'DISTRITO_L' },
        municipios: { entidad: 'municipio', dataID: 'municipio' }
    };
    switch (action.type) {
        case INIT_DATA:
            {
                var _action$payload = action.payload,
                    _action$payload$selec = _action$payload.selectedElection,
                    computed_transactions = _action$payload$selec.computed_transactions,
                    nominal_list = _action$payload$selec.nominal_list,
                    political_forces = _action$payload$selec.political_forces,
                    percentage_citizen_participation = _action$payload$selec.percentage_citizen_participation,
                    total_votes = _action$payload$selec.total_votes,
                    election_counts = _action$payload$selec.election_counts,
                    uniqueCandidates = _action$payload$selec.uniqueCandidates,
                    uniquePoliticalForces = _action$payload$selec.uniquePoliticalForces,
                    coalitions = _action$payload$selec.coalitions,
                    _action$payload$selec2 = _action$payload.selectedPlace,
                    election_type = _action$payload$selec2.election_type,
                    state_id = _action$payload$selec2.state_id,
                    year = _action$payload$selec2.year,
                    electionItems = _action$payload.electionItems,
                    partiesList = _action$payload.partiesList,
                    stateList = _action$payload.stateList;

                var mapCoalitions = {
                    political_party: function political_party(name) {
                        return [name];
                    },
                    coalition: function coalition(name) {
                        return coalitions.find(function (i) {
                            return i.coalition === name;
                        }).players;
                    }
                };
                var newFileCnfg = Object.assign({}, store.fileCnfg, {
                    file: state_id + 'mapas',
                    object: dictType[election_type.toLowerCase()].entidad,
                    dataID: [dictType[election_type.toLowerCase()].dataID]
                });
                var newElection = Object.assign({}, store.election, {
                    idplace: state_id,
                    place: stateList.find(function (i) {
                        return i.id === state_id;
                    }).denomination, //denomination,
                    year: year,
                    type: election_type
                });
                var newVoteStats = {
                    total: total_votes,
                    totalValido: computed_transactions,
                    nominal: nominal_list,
                    participacion: percentage_citizen_participation,
                    participants: election_type === 'Gobernatura' ? uniqueCandidates.length : null
                };

                var fuerzasPrc = political_forces.map(function (item) {
                    return Object.assign({}, item, {
                        political_entity: mapCoalitions[item.type](item.political_entity)
                    });
                });
                var participatingParties = uniquePoliticalForces.map(function (item) {
                    var parties = mapCoalitions[item.type](item.political_entity);
                    var party = partiesList.find(function (e) {
                        return e.acronym === parties[0];
                    });
                    return {
                        groupPolitical: item.political_entity,
                        political_entity: parties,
                        color: '#' + (party ? party.color : 'e9e9e9')
                    };
                });
                var list = election_counts.map(function (i) {
                    return Object.assign({}, i, {
                        political_forces: i.political_forces.map(function (j) {
                            var parties = mapCoalitions[j.type](j.political_entity);
                            var party = partiesList.find(function (e) {
                                return e.acronym === parties[0];
                            });
                            return Object.assign({}, j, {
                                percentage: ((j.votes || j.victories) * 100 / i.total_votes).toFixed(2),
                                groupPolitical: j.political_entity,
                                political_entity: parties,
                                color: '#' + (party ? party.color : 'e9e9e9')
                            });
                        })
                    });
                });
                return Object.assign({}, store, {
                    fileCnfg: newFileCnfg,
                    election: newElection,
                    voteStats: newVoteStats,
                    list: list,
                    partiesList: partiesList,
                    fuerzasPrc: fuerzasPrc,
                    electionItems: electionItems,
                    loading: false,
                    participatingParties: participatingParties
                });
            }
        case CHANGE_PLACE:
            {
                var _action$payload2 = action.payload,
                    _action$payload2$sele = _action$payload2.selectedPlace,
                    place = _action$payload2$sele.place,
                    idplace = _action$payload2$sele.idplace,
                    _year = _action$payload2$sele.year,
                    type = _action$payload2$sele.type,
                    _action$payload2$sele2 = _action$payload2.selectedElection,
                    _computed_transactions = _action$payload2$sele2.computed_transactions,
                    _nominal_list = _action$payload2$sele2.nominal_list,
                    _political_forces = _action$payload2$sele2.political_forces,
                    _percentage_citizen_participation = _action$payload2$sele2.percentage_citizen_participation,
                    _total_votes = _action$payload2$sele2.total_votes,
                    _election_counts = _action$payload2$sele2.election_counts,
                    _uniqueCandidates = _action$payload2$sele2.uniqueCandidates,
                    _uniquePoliticalForces = _action$payload2$sele2.uniquePoliticalForces,
                    _coalitions = _action$payload2$sele2.coalitions;
                var _partiesList = state.partiesList;


                var _mapCoalitions = {
                    political_party: function political_party(name) {
                        return [name];
                    },
                    coalition: function coalition(name) {
                        return _coalitions.find(function (i) {
                            return i.coalition === name;
                        }).players;
                    }
                };
                var _newFileCnfg = Object.assign({}, store.fileCnfg, {
                    file: idplace + 'mapas',
                    object: dictType[type.toLowerCase()].entidad,
                    dataID: [dictType[type.toLowerCase()].dataID]
                });
                var _newElection = Object.assign({}, store.election, {
                    idplace: idplace,
                    place: place,
                    type: type,
                    year: _year
                });
                var _newVoteStats = {
                    total: _total_votes,
                    totalValido: _computed_transactions,
                    nominal: _nominal_list,
                    participacion: _percentage_citizen_participation,
                    participants: type === 'Gobernatura' ? _uniqueCandidates.length : null
                };

                var _fuerzasPrc = _political_forces.map(function (item) {
                    return Object.assign({}, item, {
                        political_entity: _mapCoalitions[item.type](item.political_entity)
                    });
                });
                var _participatingParties = _uniquePoliticalForces.map(function (item) {
                    var parties = _mapCoalitions[item.type](item.political_entity);
                    var party = _partiesList.find(function (e) {
                        return e.acronym === parties[0];
                    });
                    return {
                        groupPolitical: item.political_entity,
                        political_entity: parties,
                        color: '#' + (party ? party.color : 'e9e9e9')
                    };
                });
                var _list = _election_counts.map(function (i) {
                    return Object.assign({}, i, {
                        political_forces: i.political_forces.map(function (j) {
                            var parties = _mapCoalitions[j.type](j.political_entity);
                            var party = _partiesList.find(function (e) {
                                return e.acronym === parties[0];
                            });
                            return Object.assign({}, j, {
                                percentage: ((j.votes || j.victories) * 100 / i.total_votes).toFixed(2),
                                groupPolitical: j.political_entity,
                                political_entity: parties,
                                color: '#' + (party ? party.color : 'e9e9e9')
                            });
                        })
                    });
                });

                return Object.assign({}, state, {
                    fileCnfg: _newFileCnfg,
                    election: _newElection,
                    voteStats: _newVoteStats,
                    list: _list,
                    fuerzasPrc: _fuerzasPrc,
                    loading: false,
                    participatingParties: _participatingParties
                });
            }
        default:
            return store;
    }
};

export default reducer;