import React from 'react';
import ReactDOM from 'react-dom';

import 'antd/dist/antd.css';
import './reset.css';

import Module from './module';


const App = () => (
    <Module
        match={{ path: '' }}
        isLogged
        token="eyJraWQiOiJ5XC9xdUg0OHVsYVFCTjJTN0RjeU4rTGZuM3c1akdwZ1BKYzA5WVNMMUdPaz0iLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiIxMTEwZjRjZC0wNWNlLTQ3YjktOTA5NC0yNTA5MzY2NGRlY2UiLCJpc3MiOiJodHRwczpcL1wvY29nbml0by1pZHAudXMtd2VzdC0yLmFtYXpvbmF3cy5jb21cL3VzLXdlc3QtMl95ckJERm5jWmciLCJjb2duaXRvOnVzZXJuYW1lIjoicHJpQGthcm1hcHVsc2UuY29tIiwiYXVkIjoiNnUyNW5mdm43cDk3dTVtYnBkcmFtaTJtbWYiLCJldmVudF9pZCI6ImMzYjc1OTViLWMyM2ItNGNjMC1hMDZjLTM0ZmU1OWE0MjMzZCIsImN1c3RvbTpyb2xlX25hbWUiOiJBZG1pbmlzdHJhZG9yIiwidG9rZW5fdXNlIjoiaWQiLCJjdXN0b206cm9sZV9jb2RlIjoiYWRtaW4iLCJhdXRoX3RpbWUiOjE1NzUyOTk2ODIsImV4cCI6MTU3NTMwMzI4MiwiY3VzdG9tOnJvbGVfaWQiOiI1ZDk0ZmZhZTE1ZmI1ZTM2YTdjZDQxY2EiLCJpYXQiOjE1NzUyOTk2ODIsImVtYWlsIjoicHJpQGthcm1hcHVsc2UuY29tIn0.AMDW5DFYFpGmtC41KQqRz3OvYNb8FZ0RF6FT9Bu6bI0wMonKxvYkBNBscJdN-Z3oum8txJBs6yhKAKbluKG3iBbj7NlXKv4oNvglmWZwijiE9l0lEIYEjLJGAsV4P985j4bqbJkxuj-4eGW4xVxSxNvQd5ANvJEKChVADJSrZ08CddByjFGVpJnXOEK4PDf54a94kXvexc32GfzLPyUBi4Tqn2EYEJhybdAhOL18WWLUPi550moFe8Oap9SDMR91Lt2r6t5XG9eekIFnEa2M-oxhzoOxKnVWO9CYuEr5qOGBGrM29Vl-Vu0LE8uwleii_PW6z0KGBr6Xo0rbDamF2A"
        urlService={process.env.REACT_APP_URL_SERVICE}
    />
);
const node = document.getElementById('root');

ReactDOM.render(<App />, node);
