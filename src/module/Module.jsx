import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { ApolloProvider } from '@apollo/react-hooks';
import Main from './layouts/main';
import Provider from './provider/provider';
import apolloConfig from './apolloConfig';


const Module = ({
    match: { path },
    setTitle,
    token,
    isLogged,
    urlService
}) => {
    const client = apolloConfig(urlService, token, isLogged);

    useEffect(() => {
        setTitle('Mapa - INE');
    }, []);

    return (
        <Provider>
            <ApolloProvider client={client}>
                <Router>
                    <Switch>
                        <Route exact path={path === '' ? '/' : path} component={Main} />
                    </Switch>
                </Router>
            </ApolloProvider>
        </Provider>
    );
};

const propTypes = {
    match: PropTypes.object.isRequired,
    setTitle: PropTypes.func,
    isLogged: PropTypes.bool,
    urlService: PropTypes.string,
    token: PropTypes.string,
};

Module.displayName = 'Modulo de INE'; // nombre de tu modulo
Module.propTypes = propTypes;
Module.defaultProps = {
    setTitle: () => ({}),
    isLogged: false,
    urlService: '',
    token: '',
};

export default Module;
