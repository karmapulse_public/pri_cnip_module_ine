import ApolloClient from 'apollo-client';
import { setContext } from 'apollo-link-context';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { HttpLink } from 'apollo-link-http';

const apolloConfig = (url, token, isLogged) => {
    const cache = new InMemoryCache({
        dataIdFromObject: (result) => {
            if (result._id && result.__typename) {
                if (result.__typename === 'Creator') {
                    return result.__typename + result.user_id;
                }
                return result.__typename + result._id;
            }
            return null;
        }
    });

    const httpLink = new HttpLink({
        uri: url
    });
    const authLink = setContext((_, { headers }) => {
        if (token === '' || !isLogged) {
            return {};
        }
        return {
            headers: {
                ...headers,
                authorization: `Bearer ${token}`
            }
        };
    });
    const link = authLink.concat(httpLink);

    return new ApolloClient({
        link,
        cache
    });
};

export default apolloConfig;
