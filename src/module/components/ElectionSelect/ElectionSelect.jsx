import React from 'react';
import PropTypes from 'prop-types';
import { Select } from 'antd';
import Map from './../MapChart';
import { MFCardList } from '../MainForces';
import VoteStats from '../VoteStats';
import PartiesList from '../PartiesList';
import styles from './styles';
import withData from './withData';

const { Option } = Select;

const propTypes = {
    data: PropTypes.object,
    electionItems: PropTypes.array,
    fileCnfg: PropTypes.object.isRequired,
    handleChange: PropTypes.func.isRequired,
    colorMap: PropTypes.object,
    participatingParties: PropTypes.array,
};

const defaultProps = {
    data: {},
    electionItems: [],
    colorMap: {},
    participatingParties: []
};

const ElectionSelect = (props) => {
    const {
        data: { voteStats, fuerzasPrc },
        fileCnfg,
        handleChange,
        electionItems,
        participatingParties,
        colorMap,
    } = props;

    return (
        <div className="cmp-es-card" {...styles}>
            <div className="cmp-es-card__map">
                <Select defaultValue={`${electionItems[0].election_type}-${electionItems[0].state}-${electionItems[0].year}`} onChange={handleChange} dropdownMatchSelectWidth={false}>
                    {
                        electionItems.map(selectItem => (
                            <Option
                                key={selectItem._id}
                                value={`${selectItem.election_type}-${selectItem.state}-${selectItem.year}`}
                                idplace={selectItem.state_id}
                                place={selectItem.state}
                                year={selectItem.year}
                                type={selectItem.election_type}
                            >
                                {`Elecciones ${selectItem.election_type} de ${selectItem.state} ${selectItem.year}`}
                            </Option>
                        ))
                    }
                </Select>
                <Map
                    width={315}
                    urlFile={`${process.env.REACT_APP_MAP_URL}${fileCnfg.file}.json`}
                    objectName={fileCnfg.object}
                    dataID={fileCnfg.dataID[0]}
                    groupColor={fileCnfg.dataID[0]}
                    baseStyles={{
                        default: {
                            fill: 'rgba(0, 0, 0, 0.09)', stroke: 'rgba(43, 43, 43, 0.3)', strokeWidth: '0.02px', outline: 'none', cursor: 'pointer'
                        },
                        hover: { fill: 'rgba(0, 0, 0, 0.4)', outline: 'none', cursor: 'pointer' },
                        pressed: { fill: 'rgba(0, 0, 0, 0.8)', outline: 'none', cursor: 'pointer' },
                    }}
                    colorsPerItem={colorMap}
                />
            </div>
            <div className="cmp-es-card__data">
                <MFCardList data={fuerzasPrc} />
                <VoteStats data={voteStats} />
            </div>
            <PartiesList data={participatingParties} />
        </div>
    );
};

ElectionSelect.propTypes = propTypes;
ElectionSelect.defaultProps = defaultProps;

export default withData(ElectionSelect);
