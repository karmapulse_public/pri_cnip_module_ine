import React, { useContext } from 'react';
import { Spin } from 'antd';
import { useLazyQuery } from '@apollo/react-hooks';
import { GET_ELECTION } from '../../layouts/main/queries';
import Context from '../../provider/context';
import { CHANGE_PLACE } from '../../provider/actions';


const withData = Component => (cmpProps) => {
    const [redux, dispatch] = useContext(Context);
    const {
        loading,
        voteStats,
        winnerParties,
        fuerzasPrc,
        electionItems,
        fileCnfg,
        participatingParties,
        list,
    } = redux;

    const [getElection, mapData] = useLazyQuery(GET_ELECTION, {
        onCompleted: (data) => {
            dispatch({
                type: CHANGE_PLACE,
                payload: {
                    selectedElection: data.getElectionList.electionList[0],
                    selectedPlace: mapData.variables.preData,
                }
            });
        }
    });

    const handleChange = (v, e) => {
        const { props } = e;
        const { idplace, type, year } = props;
        getElection({
            variables: {
                filter: `election_type=${type},state_id=${idplace},year=${year}`,
                preData: props
            }
        });
    };

    if (loading) {
        return (
            <div
                style={{
                    display: 'flex',
                    width: '100%',
                    height: '100%',
                    alignItems: 'center',
                    justifyContent: 'center'
                }}
            >
                <Spin
                    tip="Cargando"
                    size="large"
                />
            </div>
        );
    }

    const colorMap = {};
    list.forEach((iPlace) => {
        colorMap[iPlace.id] = { fill: iPlace.political_forces[0].color };
    });

    return (
        <Component
            {...cmpProps}
            data={{
                voteStats,
                winnerParties,
                fuerzasPrc,
            }}
            participatingParties={participatingParties}
            electionItems={electionItems}
            fileCnfg={fileCnfg}
            colorMap={colorMap}
            handleChange={handleChange}
        />
    );
};

export default withData;
