import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Tooltip } from 'antd';

import moduleStyles, { barStyles, toolTipStyles } from './styles';
import wrapperText from '../../../helpers/wrapperText';
import numberWithCommas from '../../../helpers/number';
import uniqueId from '../../../helpers/uniqueId';

const MFBarChat = (props) => {
    const { data, place } = props;
    const maxWidth = 365;
    return (
        <Fragment>
            <ul className="cmp-mf-chart" {...moduleStyles}>
                {
                    data.map(item => (
                        <li
                            key={uniqueId()}
                            className="cmp-mf-chart__item"
                        >
                            <Tooltip
                                overlayClassName={toolTipStyles(item.color).toString()}
                                title={(
                                    <Fragment>
                                        <h6>{place}</h6>
                                        <ul>
                                            <li>
                                                <span>{wrapperText(item.political_entity.join(', '), 20, 4)}</span>
                                                <span>{`${item.percentage} %`}</span>
                                            </li>
                                            <li>
                                                <span>Total de votos:</span>
                                                <span>{numberWithCommas(item.votes || item.total)}</span>
                                            </li>
                                        </ul>
                                    </Fragment>
                                )}
                            >
                                <div
                                    className="cmp-mf-chart__item__bar"
                                    {...barStyles(item.color, ((maxWidth * item.percentage) / 100))}
                                />
                            </Tooltip>
                            <p
                                className="cmp-mf-chart__item__data"
                            >
                                <span>{numberWithCommas(item.votes || item.total)} votos</span>
                                <span>{`${item.percentage}%`}</span>
                            </p>
                        </li>
                    ))
                }
            </ul>
        </Fragment>
    );
};


const propTypes = {
    data: PropTypes.array,
    place: PropTypes.string
};

const defaultProps = {
    data: [],
    place: ''
};

MFBarChat.propTypes = propTypes;
MFBarChat.defaultProps = defaultProps;

export default MFBarChat;
