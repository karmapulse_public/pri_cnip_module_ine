import { css, keyframes } from 'glamor';

export default css({
    '.cmp-mf-chart': {
        display: 'inline-flex',
        width: '365px',
        height: '145px',
        padding: '15px 40px 15px 0px',
        flexDirection: 'column',
        justifyContent: 'space-between',
    },
    ' .cmp-mf-chart': {
        '&__item': {
            display: 'flex',
            fontSize: 14,
            fontFamily: 'Roboto',
            fontWeight: 'normal',
            fontStretch: 'normal',
            fontStyle: 'normal',
            letterSpacing: 'normal',
            textAlign: 'left',
            color: '#000000',
            alignItems: 'center',
            justifyContent: 'space-between',
            ' > span:first-child': {
                opacity: 0.5,
            },
            ' > span:last-child': {
                marginLeft: 45
            },
            '&__bar': {
                display: 'inline-block',
            },
        },
        '&__item__data': {
            display: 'inline-flex',
            marginLeft: 'auto',
            fontSize: '12px',
            fontWeight: 700,
            textAlign: 'right',
            lineHeight: 1.08,
            flexDirection: 'column',
            ' > span:last-child': {
                color: 'rgba(0, 0, 0, 0.5)'
            }
        },
    },
});

const barAnimation = maxWidth => keyframes('resize', {
    '0%': {
        width: '0px',
    },
    '100%': {
        width: maxWidth
    }
});

export const barStyles = (bgColor, width) => css({
    position: 'relative',
    backgroundColor: bgColor,
    height: 13,
    animation: `${barAnimation(width)} 2s forwards`
});

export const toolTipStyles = bgColor => css({
    minWidth: '235px',
    ' .ant-tooltip-inner': {
        background: 'rgba(0, 0, 0, 0.8)',
        padding: 15,
        ' > h6': {
            marginBottom: '5px',
            fontSize: '16px',
            color: '#FFF',
            textTransform: 'capitalize'
        },
        ' > ul': {
            paddingLeft: 15,
            fontSize: '12px',
            color: 'rgba(255, 255, 255, 0.8)',
            ' > li': {
                position: 'relative',
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'space-between',
            },
        },
        ' > ul > li:first-child': {
            marginBottom: '20px',
            ':before': {
                content: '""',
                position: 'absolute',
                top: '5px',
                right: 'calc(100% + 10px)',
                width: '8px',
                height: '8px',
                backgroundColor: bgColor,
                borderRadius: '50%'
            }
        }
    },
});
