import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

import moduleStyles, { cardStyles } from './styles';
import withData from '../withData';
import wrapperText from '../../../helpers/wrapperText';
import numberWithCommas from '../../../helpers/number';
import uniqueId from '../../../helpers/uniqueId';

const MFCardList = (props) => {
    const { data, participants, type } = props;
    const dict = [
        'Primera',
        'Segunda',
        'Tercera'
    ];
    const dictType = {
        gobernatura: 'votos',
        'diputaciones locales': 'distritos',
        municipios: 'municipios'
    };

    return (
        <ul className="cmp-mf-cards" {...moduleStyles}>
            {
                data.map((item, index) => (
                    <li
                        key={uniqueId()}
                        className="cmp-mf-cards__item"
                    >
                        <span className="cmp-mf-cards__item__title">
                            {`${dict[index]} fuerza`}
                        </span>
                        <div
                            className="cmp-mf-cards__item__bar"
                            {...cardStyles(item.color)}
                        />
                        <p
                            className="cmp-mf-cards__item__name"
                        >
                            {wrapperText(item.political_entity.join(', '), 40)}
                        </p>
                        <p
                            className="cmp-mf-cards__item__data"
                        >
                            <span>{numberWithCommas(item.votes || item.victories)} {dictType[type.toLowerCase()]}</span>
                            <span>{`${item.percentage.toFixed(2)}%`}</span>
                            <span>del resultado electoral</span>
                        </p>
                    </li>
                ))
            }
            <li>
                {
                    (participants === null) ? null : (
                        <Fragment>
                            <span>
                                Total de contendientes registrados en la elección:
                            </span>
                            <span>
                                {` ${participants}`}
                            </span>
                        </Fragment>
                    )
                }
            </li>
        </ul>
    );
};


const propTypes = {
    data: PropTypes.array,
    participants: PropTypes.number,
    type: PropTypes.string
};

const defaultProps = {
    data: [],
    participants: 0,
    type: ''
};

MFCardList.propTypes = propTypes;
MFCardList.defaultProps = defaultProps;

export default withData(MFCardList);
