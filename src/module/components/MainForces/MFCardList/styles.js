import { css, keyframes } from 'glamor';

export default css({
    ' > li:last-child': {
        marginTop: '20px',
        fontSize: '14px',
        color: '#000',
        ' > span:last-child': {
            fontWeight: 700,
        }
    },
    '.cmp-mf-cards': {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
    },
    ' .cmp-mf-cards': {
        '&__item': {
            position: 'relative',
            display: 'flex',
            height: '80px',
            marginTop: '25px',
            fontFamily: 'Roboto',
            fontWeight: 'normal',
            fontStretch: 'normal',
            fontStyle: 'normal',
            letterSpacing: 'normal',
            textAlign: 'left',
            color: '#000000',
            border: 'solid 1px rgba(0, 0, 0, 0.2)',
            borderRadius: 5,
            alignItems: 'center',
            justifyContent: 'space-between',
            '&:not(:first-child)': {
                marginTop: '50px'
            },
            '&__bar': {
                width: '10px',
                height: '100%',
                borderTopLeftRadius: 5,
                borderBottomLeftRadius: 5,
                alignSelf: 'flex-start',
            },
            '&__name': {
                padding: '15px',
                textAlign: 'left',
                marginRight: 'auto',
            },
            '&__title': {
                position: 'absolute',
                bottom: 'calc(100% + 5px)',
                left: 0,
                fontSize: '14px',
                fontWeight: 700,
                lineHeight: 1.29,
                textTransform: 'uppercase'
            }
        },
        '&__item__data': {
            display: 'flex',
            height: '100%',
            minWidth: '130px',
            padding: '20px',
            textAlign: 'right',
            lineHeight: 1.08,
            borderLeft: 'solid 1px rgba(0, 0, 0, 0.09)',
            flexDirection: 'column',
            justifyContent: 'center',
            ' > span:first-child': {
                fontSize: '20px',
                color: 'rgba(0, 0, 0, 0.65)'
            },
            ' > span:not(:first-child)': {
                fontSize: '14px',
                color: 'rgba(0, 0, 0, 0.4)'
            },
        },
    },
});

const barAnimation = keyframes('resize', {
    '0%': {
        height: '0px',
    },
    '100%': {
        height: '100%'
    }
});

export const cardStyles = bgColor => css({
    position: 'relative',
    backgroundColor: bgColor,
    height: '100%',
    animation: `${barAnimation} 0.5s`
});
