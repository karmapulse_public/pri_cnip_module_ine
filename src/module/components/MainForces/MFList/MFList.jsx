import React from 'react';
import PropTypes from 'prop-types';
import moduleStyles, { dotStyles } from './styles';
import wrapperText from '../../../helpers/wrapperText';
import uniqueid from '../../../helpers/uniqueId';
// import withData from '../withData';

const MFList = (props) => {
    const { data } = props;

    return (
        <ul className="cmp-mf-list" {...moduleStyles}>
            {
                data.map((item, index) => (
                    <li
                        key={uniqueid()}
                        className="cmp-mf-list__item"
                    >
                        <span>{`${index + 1}ra. fuerza`}</span>
                        <span {...dotStyles(item.color)}>
                            {
                                wrapperText(item.political_entity.join(', '), 30)
                            }
                        </span>
                    </li>
                ))
            }
        </ul>
    );
};


const propTypes = {
    data: PropTypes.array
};

const defaultProps = {
    data: []
};

MFList.propTypes = propTypes;
MFList.defaultProps = defaultProps;

export default MFList;
