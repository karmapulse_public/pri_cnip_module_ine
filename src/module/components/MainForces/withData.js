import React, { useContext } from 'react';

import Context from '../../provider/context';

const withData = Component => (props) => {
    const [redux] = useContext(Context);
    const { data } = props;
    const { partiesList, voteStats, election: { type } } = redux;
    const mappedData = data.map((i) => {
        const party = partiesList.find(e => e.acronym === i.political_entity[0]);
        return ({
            ...i,
            color: `#${party ? party.color : 'e9e9e9'}`
        });
    });

    return (
        <Component
            data={mappedData}
            type={type}
            participants={voteStats.participants}
        />
    );
};

export default withData;
