import React from 'react';
import PropTypes from 'prop-types';
import {
    ComposableMap,
    ZoomableGroup,
    Geographies,
    Geography
} from 'react-simple-maps';
import ReactTooltip from 'react-tooltip';
import { Motion, spring } from 'react-motion';
import estilos from './styles';
import uniqueId from '../../helpers/uniqueId';
import widthData from './withData';


const propTypes = {
    // Functional props
    width: PropTypes.number,
    height: PropTypes.number,
    scale: PropTypes.number,
    // Info props
    data: PropTypes.object,
    objectName: PropTypes.string,
    dataID: PropTypes.string,
    // Style props
    groupColor: PropTypes.string,
    baseStyles: PropTypes.objectOf(PropTypes.object),
    colorsPerItem: PropTypes.objectOf(PropTypes.object),
    transformTopo: PropTypes.func.isRequired,
    zoom: PropTypes.number.isRequired,
    center: PropTypes.arrayOf(PropTypes.number).isRequired,
    ctrZoom: PropTypes.number.isRequired,
    setCtrZoom: PropTypes.func.isRequired,
    setTooltip: PropTypes.func
};

const defaultProps = {
    width: 600,
    height: 400,
    scale: 150,
    data: {},
    objectName: null,
    dataID: 'id',
    groupColor: 'group',
    baseStyles: {},
    colorsPerItem: {},
    setTooltip: () => ({})
};

const setColorByMap = (item, key, baseStyles, itemsStyles) => {
    const styles = Object.keys(item).includes(key) ? itemsStyles[item[key]] : {};
    const algo = {
        ...baseStyles,
        default: {
            ...baseStyles.default,
            ...styles
        }
    };
    return algo;
};

const Map = ({
    width,
    height,
    scale,
    zoom,
    center,
    data,
    objectName,
    groupColor,
    baseStyles,
    colorsPerItem,
    dataID,
    transformTopo,
    ctrZoom,
    setTooltip,
}) => (
    <div {...estilos}>
        <Motion
            defaultStyle={{
                zoom,
                x: center[0],
                y: center[1],
            }}
            style={{
                zoom: spring(ctrZoom < 0 ? zoom : ctrZoom, { stiffness: 100, damping: 15 }),
                x: spring(center[0], { stiffness: 100, damping: 15 }),
                y: spring(center[1], { stiffness: 100, damping: 15 }),
            }}
        >
            {perspective => (
                <ComposableMap
                    width={width}
                    height={height}
                    projection="mercator"
                    projectionConfig={{
                        scale,
                    }}
                >
                    <ZoomableGroup
                        center={[perspective.x, perspective.y]}
                        zoom={perspective.zoom}
                    >
                        <Geographies geography={transformTopo(data, objectName)}>
                            {
                                (geographies, projection) => geographies.map(item => (
                                    <Geography
                                        key={uniqueId()}
                                        // onClick={(a, b, c, d, e) => {
                                        //     // const alh = createPerspective(item);
                                        //     // console.log(data, item, a, b, c, d, e);
                                        // }}
                                        onMouseOver={() => { ReactTooltip.rebuild(); }}
                                        onFocus={() => { ReactTooltip.rebuild(); }}
                                        geography={item}
                                        projection={projection}
                                        style={
                                            setColorByMap(
                                                item.properties,
                                                groupColor,
                                                baseStyles,
                                                colorsPerItem
                                            )
                                        }
                                        data-tip={item.properties[dataID]}
                                    />
                                ))
                            }
                        </Geographies>
                    </ZoomableGroup>
                </ComposableMap>
            )}
        </Motion>
        <ReactTooltip
            type="info"
            className="map__tooltip"
            effect="solid"
            place="right"
            getContent={item => setTooltip(item)}
        />
        {/* <ZoomButtons setCtrZoom={setCtrZoom} zoom={zoom} /> */}
    </div>
);

Map.propTypes = propTypes;
Map.defaultProps = defaultProps;

export default widthData(Map);
