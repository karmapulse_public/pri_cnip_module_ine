import React from 'react';
import PropTypes from 'prop-types';
import { faPlus, faMinus } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';


const propTypes = {
    // Functional props
    setCtrZoom: PropTypes.func,
    zoom: PropTypes.number
};

const defaultProps = {
    setCtrZoom: () => ({}),
    zoom: 7
};

const Map = ({
    setCtrZoom,
    zoom
}) => (
    <div className="map__zoom-buttons">
        <button
            onClick={() => { setCtrZoom(zoom + 10); }}
        >
            <FontAwesomeIcon icon={faPlus} />
        </button>
        <button
            onClick={() => { setCtrZoom(zoom - 10); }}
        >
            <FontAwesomeIcon icon={faMinus} />
        </button>
    </div>
);

Map.propTypes = propTypes;
Map.defaultProps = defaultProps;

export default Map;
