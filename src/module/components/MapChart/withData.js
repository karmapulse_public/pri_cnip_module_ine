import React, { useState, useEffect, useContext } from 'react';
import { Spin } from 'antd';
import fetch from 'isomorphic-fetch';
import { feature, bbox } from 'topojson-client';
import Tooltip from '../TooltipPlace';
import Context from '../../provider/context';

const withData = Component => (props) => {
    const [redux] = useContext(Context);
    const { urlFile = null, width = 600, height = 500 } = props;
    const [topoData, setTopoData] = useState({});
    const [status, setStatus] = useState(0); // x < 0: Error; x = 0: Loading; x > 0: Data
    const [ctrZoom, setCtrZoom] = useState(-1);

    // GET DATA - TOPOJSONS
    useEffect(() => {
        setStatus(0);
        fetch(urlFile)
            .then(response => response.json())
            .then((topoState) => {
                setTopoData(topoState);
                setStatus(1);
            }).catch(() => { setStatus(-1); });
    }, [urlFile, setTopoData, setStatus]);

    /* Functions */
    const transformTopo = (topo, key) => {
        if (!topo || Object.keys(topo).length === 0) return [];
        return feature(
            topo,
            topo.objects[key] || {}
        ).features;
    };

    const createPerspective = (topo) => {
        if (Object.keys(topo).length === 0) return { center: [19.432608, -99.133209], zoom: 7 };
        const newTopo = { ...topo };
        newTopo.bbox = newTopo.bbox || bbox(newTopo);
        const dx = newTopo.bbox[2] + newTopo.bbox[0];
        const dy = newTopo.bbox[1] + newTopo.bbox[3];
        const ddx = newTopo.bbox[2] - newTopo.bbox[0];
        const ddy = newTopo.bbox[3] - newTopo.bbox[1];
        return {
            center: [dx / 2, dy / 2],
            zoom: (0.3 / Math.max(ddx / width, ddy / height))
        };
    };

    const setTooltip = (id) => {
        if (!id) return null;
        const { list } = redux;
        const itemMap = list.find(i => i.id === parseInt(id, 10));
        if (!itemMap) return null;
        const percentage = ((itemMap.political_forces[0].votes || itemMap.political_forces[0].victories) * 100) / itemMap.total_votes;
        return (
            <Tooltip
                place={itemMap.denomination}
                porcentage={percentage.toFixed(2)}
                total={itemMap.total_votes}
                partidos={itemMap.political_forces[0].political_entity}
                color={itemMap.political_forces[0].color}
            />
        );
    };
    /* Functions */

    const tempPers = createPerspective(topoData);
    let { zoom } = tempPers;
    const { center } = tempPers;
    zoom = ctrZoom < 0 ? zoom : ctrZoom;

    if (status < 0) {
        return null;
    }
    if (status === 0) {
        return (
            <div
                style={{
                    display: 'flex',
                    width: '100%',
                    height: '100%',
                    alignItems: 'center',
                    justifyContent: 'center'
                }}
            >
                <Spin
                    tip="Cargando"
                    size="large"
                />
            </div>
        );
    }
    if (status > 0) {
        return (
            <Component
                data={topoData}
                zoom={zoom}
                center={center}
                transformTopo={transformTopo}
                ctrZoom={ctrZoom}
                setCtrZoom={setCtrZoom}
                setTooltip={setTooltip}
                {...props}
            />
        );
    }
    return null;
};

export default withData;
