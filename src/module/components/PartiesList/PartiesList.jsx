import React from 'react';
import PropTypes from 'prop-types';

import uniqueId from '../../helpers/uniqueId';
import styles, { partyStyles } from './styles';

const propTypes = {
    data: PropTypes.arrayOf(PropTypes.object)
};

const defaultProps = {
    data: [{}]
};

const PartiesList = (props) => {
    const { data } = props;
    return (
        <ul className="cmp-pl-container" {...styles}>
            {
                data.map(item => (
                    <li
                        className="cmp-pl-container__item"
                        {...partyStyles(item.color)}
                        key={uniqueId()}
                    >
                        {item.political_entity.join(', ')}
                    </li>
                ))
            }
        </ul>
    );
};

PartiesList.propTypes = propTypes;
PartiesList.defaultProps = defaultProps;

export default PartiesList;
