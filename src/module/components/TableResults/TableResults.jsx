import React from 'react';
import PropTypes from 'prop-types';
import { Table } from 'antd';
import withData from './withData';
import styles from './styles';

const TableResults = (props) => {
    const { data, columns } = props;

    return (
        <Table
            bordered
            align="center"
            columns={columns}
            dataSource={data}
            rowKey="id"
            pagination={{
                showSizeChanger: true,
                hideOnSinglePage: true,
                locale: { items_per_page: ' registros ' }
            }}
            {...styles}
        />
    );
};

const propTypes = {
    columns: PropTypes.arrayOf(PropTypes.object),
    data: PropTypes.array
};

const defaultProps = {
    columns: [],
    data: []
};

TableResults.propTypes = propTypes;
TableResults.defaultProps = defaultProps;


export default withData(TableResults);
