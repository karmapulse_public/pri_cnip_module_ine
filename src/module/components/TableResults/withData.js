import React, { useContext } from 'react';
import { MFList, MFBarChart } from '../MainForces';
import numberWithCommas from '../../helpers/number';
// import wrapperText from '../../helpers/wrapperText';
import Context from '../../provider/context';

const withData = Component => () => {
    const [redux] = useContext(Context);
    const { list, election: { type } } = redux;
    const dictType = {
        gobernatura: 'Municipio',
        'diputaciones locales': 'Distrito Local',
        municipios: 'Municipio'
    };
    const columns = [
        {
            title: dictType[type.toLowerCase()],
            dataIndex: 'denomination',
            key: 'denomination',
            width: 175,
            render: (name, row) => <p className="tbl-rslts__name">{`${row.id}. ${name}`}</p>,
            sorter: (a, b) => a.id - b.id,
        },
        {
            title: 'Fuerza electoral',
            dataIndex: 'political_forces',
            key: 'id',
            width: 765,
            render: (objectData, row) => {
                return (
                    <div className="tbl-rslts__main-forces">
                        <MFList data={objectData} />
                        <MFBarChart data={objectData} place={row.denomination} />
                    </div>
                );
            },
        },
        {
            title: 'Total de votos',
            dataIndex: 'total_votes',
            key: 'total_votes',
            render: number => <p className="tbl-rslts__total-votes">{numberWithCommas(number)}</p>,
            sorter: (a, b) => a.total_votes - b.total_votes,
        },
    ];

    return (
        <Component
            data={list}
            columns={columns}
        />
    );
};

export default withData;
