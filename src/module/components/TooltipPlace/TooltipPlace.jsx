import React from 'react';
import PropTypes from 'prop-types';
import wrapperText from '../../helpers/wrapperText';
import numberWithCommas from '../../helpers/number';
import styles from './styles';

const TooltipPlace = (props) => {
    const {
        place,
        partidos,
        porcentage,
        total,
        color
    } = props;

    return (
        <div {...styles(color)}>
            <h6>{place}</h6>
            <ul>
                <li>
                    <span>{wrapperText(partidos.join(', '), 20, 4)}</span>
                    <span>{`${porcentage} %`}</span>
                </li>
                <li>
                    <span>Total de votos:</span>
                    <span>{numberWithCommas(total)}</span>
                </li>
            </ul>
        </div>
    );
};

const propTypes = {
    place: PropTypes.string,
    partidos: PropTypes.arrayOf(PropTypes.string),
    porcentage: PropTypes.string,
    total: PropTypes.number,
    color: PropTypes.string
};

const defaultProps = {
    place: 'Localidad',
    partidos: ['Partidos'],
    porcentage: '0',
    total: 0,
    color: '#FFF'
};

TooltipPlace.propTypes = propTypes;
TooltipPlace.defaultProps = defaultProps;


export default TooltipPlace;
