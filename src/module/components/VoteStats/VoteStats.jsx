import React from 'react';
import PropTypes from 'prop-types';

import styles from './styles';
import numberWithCommas from '../../helpers/number';

const VoteStats = (props) => {
    const { data } = props;
    return (
        <ul
            className="cmp-vs-container"
            {...styles}
        >
            <li
                className="cmp-vs-container__item"
            >
                <h4>Total de Votos</h4>
                <span>{numberWithCommas(data.total)}</span>
            </li>
            <li
                className="cmp-vs-container__item"
            >
                <h4>Votación válida</h4>
                <span>{numberWithCommas(data.totalValido)}</span>
            </li>
            <li
                className="cmp-vs-container__item"
            >
                <h4>Lista Nominal</h4>
                <span>{numberWithCommas(data.nominal)}</span>
            </li>
            <li
                className="cmp-vs-container__item"
            >
                <h4>Porcentaje de Participación</h4>
                <span>{data.participacion.toFixed(2)}%</span>
            </li>
        </ul>
    );
};

const propTypes = {
    data: PropTypes.objectOf(PropTypes.number)
};

const defaultProps = {
    data: {}
};

VoteStats.propTypes = propTypes;
VoteStats.defaultProps = defaultProps;

export default VoteStats;
