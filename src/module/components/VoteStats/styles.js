import { css } from 'glamor';

export default css({
    '.cmp-vs-container': {
        display: 'flex',
        paddingTop: '15px',
        borderTop: 'solid 1px rgba(0, 0, 0, 0.2)',
        justifyContent: 'space-between'
    },
    ' .cmp-vs-container__item': {
        fontFamily: 'Roboto',
        fontSize: '14px',
        color: '#000',
        ' > h4': {
            opacity: 0.4
        },
        ' > span': {
            fontSize: '20px',
            opacity: 0.65,
            fontWeight: 700
        }
    },
});
