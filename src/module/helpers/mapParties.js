const mapParties = (coalitions, keyList, keyValue, value, type) => {
    if (type === 'coalition') {
        return (coalitions.find(item => item[keyList] === value)[keyValue]);
    }
    if (type === 'political_party') {
        return [value];
    }
};

export default mapParties;
