import React, { Fragment } from 'react';
import uniqueid from './uniqueId';

const wrapperText = (rawText, maxWidth, maxHeight = 1) => {
    if (rawText.length <= maxWidth) return rawText;

    let i = 0;
    let f = 0;
    const listSegment = [];
    // Pre procesing
    while (i + maxWidth < rawText.length && f < maxHeight) {
        const rawSegment = rawText.substring(i, i + maxWidth);
        let j = i + maxWidth;
        if (rawText[j] !== ' ' && rawText[j - 1] !== ' ') {
            j -= 1;
            while (rawText[j] !== ' ') {
                j -= 1;
            }
            j += 1;
        }
        const remainder = i - (j - maxWidth);
        listSegment.push(remainder > 0 ?
            rawText.substring(i, (i + (maxWidth - remainder))) :
            rawSegment);
        i = remainder > 0 ? i - (remainder) : i;
        i += maxWidth;
        f += 1;
    }
    if (f < maxHeight) {
        listSegment.push(rawText.substring(i, i + maxWidth));
    }
    f = 0;
    const lastItem = listSegment.pop();
    return (
        <Fragment>
            {listSegment.map(item => (<Fragment key={uniqueid()}>{item}<br /></Fragment>))}
            {`${lastItem}...`}
        </Fragment>
    );
};

export default wrapperText;
