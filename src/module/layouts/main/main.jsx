import React from 'react';
import PropTypes from 'prop-types';
import TableResults from '../../components/TableResults';
import ElectionSelect from '../../components/ElectionSelect';
import styles from './styles';
import withData from './withData';

const propTypes = {
    place: PropTypes.string,
    type: PropTypes.string,
    year: PropTypes.number
};

const defaultProps = {
    place: '',
    type: '',
    year: 0
};

const main = (props) => {
    const { place, type, year } = props;
    const dictType = {
        gobernatura: 'municipio',
        'diputaciones locales': 'distrito',
        municipios: 'municipio'
    };
    return (
        <div className="lyt-main" {...styles}>
            <div className="lyt-main__title">
                <h1>{`ELECCIÓN ${place} ${year > 0 ? year : ''}`}</h1>
            </div>
            <ElectionSelect />
            <div className="lyt-main__subtitle">
                <h2>Detalles de la elección por locación</h2>
                <p>
                    {`Lista de las principales fuerzas por número de votos, porcentaje de votación y total de votos del ${dictType[type.toLowerCase()]}.`}
                </p>
            </div>
            <TableResults />
        </div>
    );
};

main.propTypes = propTypes;
main.defaultProps = defaultProps;

export default withData(main);
