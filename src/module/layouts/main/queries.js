import { gql } from 'apollo-boost';

export const GET_ELECTION_LIST = gql`
    query getElectionList(
        $sort: String
    ){
        getElectionList(sort: $sort) {
            electionList {
                _id,
                denomination,
                election_type,
                state_id,
                year
            }
        }
        getStateList {
            stateList {
                _id,
                id,
                denomination
            }
        }
        getPoliticalPartiesList {
            acronym
            color
        }
    }
`;

export const GET_ELECTION = gql`
    query getElection(
        $filter: String
    ){
        getElectionList(filter: $filter) {
            electionList {
                _id,
                computed_transactions,
                nominal_list,
                percentage_citizen_participation,
                total_votes,
                uniqueCandidates,
                uniquePoliticalForces {
                    political_entity,
                    type
                },
                election_counts {
                    id,
                    denomination,
                    total_votes,
                    political_forces {
                        votes,
                        political_entity,
                        victories,
                        type
                    }
                },
                coalitions {
                    coalition,
                    name,
                    players
                },
                political_forces {
                    votes,
                    political_entity,
                    victories,
                    type,
                    percentage
                }
            }
        }
    }
`;
