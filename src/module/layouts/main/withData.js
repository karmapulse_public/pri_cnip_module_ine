import React, { useContext } from 'react';

import { useQuery, useLazyQuery } from '@apollo/react-hooks';
import Context from '../../provider/context';
import { GET_ELECTION_LIST, GET_ELECTION } from './queries';
import { INIT_DATA } from '../../provider/actions';

const withData = Component => () => {
    const [redux, dispatch] = useContext(Context);

    const [getElection, mapData] = useLazyQuery(GET_ELECTION, {
        onCompleted: (data) => {
            dispatch({
                type: INIT_DATA,
                payload: {
                    selectedElection: data.getElectionList.electionList[0],
                    selectedPlace: mapData.variables.preData,
                    electionItems: mapData.variables.electionItems,
                    stateList: mapData.variables.stateList,
                    partiesList: mapData.variables.partiesList,
                }
            });
        }
    });

    useQuery(GET_ELECTION_LIST, {
        fetchPolicy: 'network-only',
        variables: { sort: 'state_id,year' },
        onCompleted: (data) => {
            if (data) {
                const { getElectionList: { electionList }, getStateList: { stateList }, getPoliticalPartiesList } = data;
                const { election_type, state_id, year } = electionList[0];
                const selectData = electionList.map(elecItem => ({
                    ...elecItem,
                    state: stateList.find(i =>
                        (i.id === elecItem.state_id)).denomination
                }));
                getElection({
                    variables: {
                        filter: `election_type=${election_type},state_id=${state_id},year=${year}`,
                        preData: electionList[0],
                        electionItems: selectData,
                        partiesList: getPoliticalPartiesList,
                        stateList
                    }
                });
            }
        }
    });

    const { election: { place, type, year } } = redux;

    return (
        <Component
            place={place}
            type={type}
            year={year}
        />
    );
};

export default withData;
