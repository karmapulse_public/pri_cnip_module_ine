import React from 'react';
import PropTypes from 'prop-types';
import Context from './context';
import reducer from './reducer';
import store from './store';

const Provider = (props) => {
    const contextValue = React.useReducer(reducer, store);
    return (
        <Context.Provider value={contextValue}>
            {props.children}
        </Context.Provider>
    );
};

Provider.propTypes = {
    children: PropTypes.any.isRequired
};

export default Provider;
