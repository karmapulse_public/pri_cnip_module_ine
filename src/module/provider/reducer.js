import store from './store';

import {
    INIT_DATA,
    CHANGE_PLACE
} from './actions';

const reducer = (state, action) => {
    const dictType = {
        gobernatura: { entidad: 'municipio', dataID: 'municipio' },
        'diputaciones locales': { entidad: 'local', dataID: 'DISTRITO_L' },
        municipios: { entidad: 'municipio', dataID: 'municipio' },
    };
    switch (action.type) {
    case INIT_DATA: {
        const {
            selectedElection: {
                computed_transactions,
                nominal_list,
                political_forces,
                percentage_citizen_participation,
                total_votes,
                election_counts,
                uniqueCandidates,
                uniquePoliticalForces,
                coalitions,
            },
            selectedPlace: {
                // denomination,
                election_type,
                state_id,
                year
            },
            electionItems,
            partiesList,
            stateList
        } = action.payload;
        const mapCoalitions = {
            political_party: name => [name],
            coalition: name => coalitions.find(i => i.coalition === name).players
        };
        const newFileCnfg = {
            ...store.fileCnfg,
            file: `${state_id}mapas`,
            object: dictType[election_type.toLowerCase()].entidad,
            dataID: [dictType[election_type.toLowerCase()].dataID]
        };
        const newElection = {
            ...store.election,
            idplace: state_id,
            place: stateList.find(i => i.id === state_id).denomination, //denomination,
            year,
            type: election_type
        };
        const newVoteStats = {
            total: total_votes,
            totalValido: computed_transactions,
            nominal: nominal_list,
            participacion: percentage_citizen_participation,
            participants: election_type === 'Gobernatura' ? uniqueCandidates.length : null
        };

        const fuerzasPrc = political_forces.map(item => ({
            ...item,
            political_entity: mapCoalitions[item.type](item.political_entity)
        }));
        const participatingParties = uniquePoliticalForces.map((item) => {
            const parties = mapCoalitions[item.type](item.political_entity);
            const party = partiesList.find(e => e.acronym === parties[0]);
            return {
                groupPolitical: item.political_entity,
                political_entity: parties,
                color: `#${party ? party.color : 'e9e9e9'}`
            };
        });
        const list = election_counts.map(i => ({
            ...i,
            political_forces: i.political_forces.map((j) => {
                const parties = mapCoalitions[j.type](j.political_entity);
                const party = partiesList.find(e => e.acronym === parties[0]);
                return {
                    ...j,
                    percentage: (((j.votes || j.victories) * 100) / i.total_votes).toFixed(2),
                    groupPolitical: j.political_entity,
                    political_entity: parties,
                    color: `#${party ? party.color : 'e9e9e9'}`
                };
            })
        }));
        return {
            ...store,
            fileCnfg: newFileCnfg,
            election: newElection,
            voteStats: newVoteStats,
            list,
            partiesList,
            fuerzasPrc,
            electionItems,
            loading: false,
            participatingParties,
        };
    }
    case CHANGE_PLACE: {
        const {
            selectedPlace: {
                place,
                idplace,
                year,
                type
            },
            selectedElection: {
                computed_transactions,
                nominal_list,
                political_forces,
                percentage_citizen_participation,
                total_votes,
                election_counts,
                uniqueCandidates,
                uniquePoliticalForces,
                coalitions,
            },
        } = action.payload;
        const {
            partiesList
        } = state;

        const mapCoalitions = {
            political_party: name => [name],
            coalition: name => coalitions.find(i => i.coalition === name).players
        };
        const newFileCnfg = {
            ...store.fileCnfg,
            file: `${idplace}mapas`,
            object: dictType[type.toLowerCase()].entidad,
            dataID: [dictType[type.toLowerCase()].dataID]
        };
        const newElection = {
            ...store.election,
            idplace,
            place,
            type,
            year
        };
        const newVoteStats = {
            total: total_votes,
            totalValido: computed_transactions,
            nominal: nominal_list,
            participacion: percentage_citizen_participation,
            participants: type === 'Gobernatura' ? uniqueCandidates.length : null
        };

        const fuerzasPrc = political_forces.map(item => ({
            ...item,
            political_entity: mapCoalitions[item.type](item.political_entity)
        }));
        const participatingParties = uniquePoliticalForces.map((item) => {
            const parties = mapCoalitions[item.type](item.political_entity);
            const party = partiesList.find(e => e.acronym === parties[0]);
            return {
                groupPolitical: item.political_entity,
                political_entity: parties,
                color: `#${party ? party.color : 'e9e9e9'}`
            };
        });
        const list = election_counts.map(i => ({
            ...i,
            political_forces: i.political_forces.map((j) => {
                const parties = mapCoalitions[j.type](j.political_entity);
                const party = partiesList.find(e => e.acronym === parties[0]);
                return {
                    ...j,
                    percentage: (((j.votes || j.victories) * 100) / i.total_votes).toFixed(2),
                    groupPolitical: j.political_entity,
                    political_entity: parties,
                    color: `#${party ? party.color : 'e9e9e9'}`
                };
            })
        }));

        return {
            ...state,
            fileCnfg: newFileCnfg,
            election: newElection,
            voteStats: newVoteStats,
            list,
            fuerzasPrc,
            loading: false,
            participatingParties,
        };
    }
    default:
        return store;
    }
};

export default reducer;
