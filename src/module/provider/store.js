export default {
    loading: true,
    electionItems: [],
    fileCnfg: {
        file: '',
        object: '',
        dataID: []
    },
    election: {
        place: '',
        type: '',
        year: 0
    },
    fuerzasPrc: [],
    list: [],
    voteStats: {
        total: 0,
        totalValido: 0,
        nominal: 0,
        participacion: 0,
        participantes: 0
    },
    participatingParties: [],
    partiesList: []

};